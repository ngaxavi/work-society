/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package org.jhipster.worksociety.web.rest.dto;
