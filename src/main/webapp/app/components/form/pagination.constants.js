(function() {
    'use strict';

    angular
        .module('workSocietyApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
