(function () {
    'use strict';

    angular
        .module('workSocietyApp')
        .controller('CiStatusController', CiStatusController);

    CiStatusController.$inject = ['$interval', 'CiStatusService'];

    function CiStatusController($interval, CiStatusService) {
        var vm = this;

        var stop;

        stop = $interval(get, 2000);

        function get() {
            if (vm.status === undefined || angular.equals(vm.status, "pending")) {
                getStatus('master');
            } else {
                $interval.cancel(stop);
            }
        }

        // getStatus('master');
        //
        // if (angular.equals(vm.status, "success")) {
        //     $interval(function () {
        //         getStatus('master');
        //     }, 20000);
        // }


        function getStatus(branch) {
            //  get the current/ last commit
            CiStatusService.getLastCommit(branch).then(function (lastCommitSha) {
                // the found commit sha use to retrieve it status
                CiStatusService.getStatus(lastCommitSha).then(function (status) {
                    vm.status = status;
                });
            });
        }
    }
})();
