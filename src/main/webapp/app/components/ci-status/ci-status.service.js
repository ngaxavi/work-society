(function () {

    'use strict';

    angular
        .module('workSocietyApp')
        .factory('CiStatusService', CiStatusService);

    CiStatusService.$inject = ['$http'];

    function CiStatusService($http) {
        return {
            getStatus: getStatus,
            getLastCommit: getLastCommit
        };

        function getStatus(branch) {
            return $http.get('http://gitlab.com/api/v3/projects/1286629/repository/commits/' +
                branch + '?private_token=7ZzHxvGj93g-b1BHwCps')
                .then(function (response) {
                    console.log(response.data.status);
                    return response.data.status;
                });
        }

        function getLastCommit(branch) {
            return $http.get('http://gitlab.com/api/v3/projects/1286629/repository/commits?private_token=7ZzHxvGj93g-b1BHwCps&ref_name=' + branch)
                .then(function (response) {
                    return response.data[0].short_id;
                });
        }
    }
})();
